﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using satiyorum.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace satiyorum.Controllers
{
    public class HomeController : Controller
    {

        private SatiyorumDbContext db = new SatiyorumDbContext();
        public kullaniciManager kullaniciYonetim
        {
            get
            {
                IOwinContext context = HttpContext.GetOwinContext();
                return context.GetUserManager<kullaniciManager>();
            }
        }

        public ActionResult Index()
        {
            IndexViewModel model = new IndexViewModel();
            model.kategori = db.Ilanlar.Take(12);
            model.kampanya = db.Kampanya.Take(3);
            return View(model);
        }

        public ActionResult Kategori(string kat)
        {
            ViewBag.sinif = kat;
            return View();
        }
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.url = returnUrl;

            return View(new KullaniciGirisModel());
        }
        [HttpPost]
        public async Task<ActionResult> Login(KullaniciGirisModel user, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                kullanici kullaniciName = await kullaniciYonetim.FindByNameAsync(user.KullaniciAdi);

                if (kullaniciName == null)
                {
                    ModelState.AddModelError("", "Kullanıcı hatalı");
                }
                else
                {
                    kullanici CurrentKullanici = await kullaniciYonetim.FindAsync(kullaniciName.UserName, user.Sifre);
                    //kullaniciManager.FindAsync(user);
                    if (CurrentKullanici == null)
                    {
                        ModelState.AddModelError("", "Şifre hatalı");
                    }
                    else
                    {
                        ClaimsIdentity ident = await kullaniciYonetim.CreateIdentityAsync(CurrentKullanici, DefaultAuthenticationTypes.ApplicationCookie);
                        HttpContext.GetOwinContext().Authentication.SignOut();
                        HttpContext.GetOwinContext().Authentication.SignIn(new AuthenticationProperties() { IsPersistent = true }, ident);
                    }
                }
            }
            if (returnUrl != null)
            {
                Response.Redirect(returnUrl);
            }
            else
            {
                Response.Redirect("index");
            }
            return View(user);
        }
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Register(KullaniciKayitModel k)
        {
            ViewBag.Panel = "admin";
            kullanici myKullanici = new kullanici();
            myKullanici.Email = k.Email;
            myKullanici.UserName = k.UserName;
            myKullanici.password = k.Sifre;
            myKullanici.Name = k.Name;
            myKullanici.SurName = k.Surname;
            myKullanici.PhoneNumber = k.PhoneNumber;
            IdentityResult result = await kullaniciYonetim.CreateAsync(myKullanici, myKullanici.password);
            if (result.Succeeded)
            {
                return RedirectToAction("Kullanici");
            }
            else
            {
                return View();
            }
        }

        public ActionResult Ilan()
        {
            return View();
        }
        public ActionResult DilDegis(string LanguageAbbrevation)
        {
            if(LanguageAbbrevation!=null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(LanguageAbbrevation);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(LanguageAbbrevation);
            }
            return View();
        }
    }
}